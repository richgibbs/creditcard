# Credit Card System

After runnig `npm i` all dependencies should be installed, however it may be necessary to manually install fsevents. So if any events errors occur at runtime then run

`npm install fsevents@1.2.11`

## Input Data

From the spec it wasn't clear how to obtain the balances for the cards so I've provided a data.json file to simulate the data that might arrive from the server. This is a list of cards and their balances. It can be added to using this client although any additions are not persisted to the file once the app is stopped.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
