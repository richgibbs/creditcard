import { useState, useEffect } from 'react';

export const useForm = (initialValues, validate, callback) => {
  const [values, setValues] = useState(initialValues);
  const [errors, setErrors] = useState({});
  const [isSubmitting, setIsSubmitting] = useState(false);

  useEffect(() => {
    // console.log('errors changed isSubmitting ', isSubmitting);
    if (Object.keys(errors).length === 0 && isSubmitting) {
      callback();
    }
    setIsSubmitting(false);
  }, [errors, isSubmitting, callback]);

  const handleSubmit = event => {
    setIsSubmitting(true);
    setErrors(validate(values));
  };

  const handleChange = e => {
    setValues({
      ...values,
      [e.target.name]: e.target.value
    });
  };

  return { values, handleChange, handleSubmit, errors, isSubmitting };
};
