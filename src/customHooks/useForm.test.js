import { useForm } from './useForm';
import { renderHook, act } from '@testing-library/react-hooks';

describe('useForm', () => {
  it('should set the default values', () => {
    // Instantiate the hook using the renderHook
    const { result } = renderHook(() =>
      useForm({ name: 'ya', cardNumber: '1234' })
    );
    expect(result.current.values.name).toBe('ya');
    expect(result.current.values.cardNumber).toBe('1234');
  });

  it('should set the values on handleChange', () => {
    // dummy event to be passed to the handleChange
    const event = {
      target: {
        name: 'name',
        value: 'ff'
      }
    };

    // Instantiate the hook using the renderHook
    const { result } = renderHook(() => useForm({ name: '', cardNumber: '' }));

    // console.log('result is = ', result.current);
    // call the handleChange with the example payload
    act(() => {
      result.current.handleChange(event);
    });
    expect(result.current.values.name).toBe('ff');
  });

  it('should set the errors onSubmit', () => {
    // dummy event to be passed to the handleSubmit
    const event = {};
    const dummyErrors = { name: 'required' };
    const validate = values => dummyErrors;
    const callBack = () => true;

    // Instantiate the hook using the renderHook
    const { result } = renderHook(() =>
      useForm({ name: 'joe', cardNumber: '1234' }, validate, callBack)
    );

    // call the handleSubmit
    act(() => {
      result.current.handleSubmit(event);
    });
    // console.log('result is = ', result.current);
    expect(result.current.errors).toBe(dummyErrors);
  });

  it('should return no errors onSubmit', () => {
    const event = {};
    const noErrors = {};
    const validate = values => noErrors;
    const callBack = () => true;

    // Instantiate the hook using the renderHook
    const { result } = renderHook(() =>
      useForm({ name: 'joe', cardNumber: '1234' }, validate, callBack)
    );

    // call the handleSubmit
    act(() => {
      result.current.handleSubmit(event);
    });
    expect(result.current.errors).toBe(noErrors);
  });
});
