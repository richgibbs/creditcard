import React, { useState } from 'react';
import './App.css';
import AddCard from './components/addCard/AddCard';
import DisplayCards from './components/displayCards/DisplayCards';
import cardData from './data.json';
const Ajv = require('ajv');
const ajv = new Ajv();

const dataSchema = require('./dataSchema.json');

const validate = (schema, data) => {
  return ajv.validate(schema, data) ? true : ajv.errors;
};

const validJson = validate(dataSchema, cardData);

const App = () => {
  const [rows, setRows] = useState(cardData);

  if (typeof validJson === 'object') {
    return (
      <div className="json-error">
        <h2>Cannot load page - Invalid input JSON</h2>
        <p>{JSON.stringify(validJson)}</p>
      </div>
    );
  }

  return (
    <div className="App">
      <h1>Credit Card System</h1>
      <AddCard
        submitData={data => {
          setRows(currentRows => [{ ...data }, ...currentRows]);
        }}
      />
      <DisplayCards data={rows} />
    </div>
  );
};

export default App;
