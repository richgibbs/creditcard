const validateCard = values => {
  let errors = {};

  // Name errors
  if (!values.name) {
    errors.name = 'Required';
    console.log('error name required');
  }

  // CardNumber errors
  if (!values.cardNumber) {
    errors.cardNumber = 'Required';
  } else if (/^(0|[1-9]\d*)$/.test(values.cardNumber) === false) {
    errors.cardNumber = 'Card can only contain digits 0 to 9';
  } else if (values.cardNumber.length > 19) {
    errors.cardNumber = 'Card number cannot be greater than 19 digits';
  }

  // Limit errors
  if (!values.limit) {
    errors.limit = 'Required';
  } else if (/^\d+(\.\d+)?$/.test(values.limit) === false) {
    errors.limit = 'Limit must be a positive number';
  }

  return errors;
};

export default validateCard;
