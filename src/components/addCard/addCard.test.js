import React from 'react';
import { shallow } from 'enzyme';
import AddCard from './AddCard';

describe('<AddCard />', () => {
  const container = shallow(<AddCard />);

  it('should have an name input field', () => {
    expect(container.find('input[name="name"]').length).toEqual(1);
  });

  it('should have an cardNumber input field', () => {
    expect(container.find('input[name="cardNumber"]').length).toEqual(1);
  });

  it('should have an limit input field', () => {
    expect(container.find('input[name="limit"]').length).toEqual(1);
  });

  it('should have a form defined', () => {
    expect(container.find('Form')).toBeDefined();
  });

  // it('should increment counter when clicking button', () => {
  //   const initialCounter = container.find('[name="counter"]').text();
  //   expect(initialCounter).toBe('Count: 10');

  //   const button = container.find('button');
  //   button.simulate('click');
  //   const subsequentCounter = container.find('[name="counter"]').text();
  //   expect(subsequentCounter).toBe('Count: 11');
  // });
});
