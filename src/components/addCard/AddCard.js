import React from 'react';
import './addCard.css';
import { useForm } from '../../customHooks/useForm';
import validateCard from './validateCard';

const AddCard = props => {
  const commitData = () => {
    values.balance = 0;
    props.submitData(values);
    values.name = '';
    values.cardNumber = '';
    values.limit = '';
  };

  const { values, handleChange, errors, handleSubmit } = useForm(
    { name: '', cardNumber: '', limit: '' },
    validateCard,
    commitData
  );

  const submitForm = e => {
    e.preventDefault();
    handleSubmit();
  };

  return (
    <>
      <h3>Add Card</h3>
      <form onSubmit={submitForm}>
        <label htmlFor={values.name}>Name</label>
        <input
          className="input"
          name="name"
          value={values.name}
          onChange={handleChange}
        ></input>
        {errors.name && <span className="error">{errors.name}</span>}
        <label htmlFor={values.cardNumber}>Card Number</label>
        <input
          className="input"
          name="cardNumber"
          value={values.cardNumber}
          onChange={handleChange}
        ></input>
        {errors.cardNumber && (
          <span className="error">{errors.cardNumber}</span>
        )}
        <label htmlFor={values.limit}>Limit</label>
        <input
          className="input"
          name="limit"
          value={values.limit}
          onChange={handleChange}
        ></input>
        {errors.limit && <span className="error">{errors.limit}</span>}
        <input className="submit-btn" type="submit" value="Add" />
        {/* <button type="submit">Add</button> */}
      </form>
    </>
  );
};

export default AddCard;
