import React from 'react';
import './displayCards.css';

const formatCardNumber = number => {
  return number.replace(/(.{4})/g, '$1 ');
};

const DisplayCards = props => {
  const tableRows = props.data.map(card => {
    return (
      <tr key={card.cardNumber}>
        <td>{card.name}</td>
        <td>{formatCardNumber(card.cardNumber)}</td>
        <td className={card.balance < 0 ? 'negative-balance' : null}>
          {card.balance}
        </td>
        <td>{card.limit}</td>
      </tr>
    );
  });

  return (
    <table>
      <thead>
        <tr>
          <th>Name</th>
          <th>Card Number</th>
          <th>Balance</th>
          <th>Limit</th>
        </tr>
      </thead>
      <tbody>{tableRows}</tbody>
    </table>
  );
};

export default DisplayCards;
