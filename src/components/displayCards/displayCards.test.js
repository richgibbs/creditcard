import React from 'react';
import { shallow } from 'enzyme';
import DisplayCards from './DisplayCards';

const rows = [
  {
    name: 'joe',
    cardNumber: '12345',
    balance: 12,
    limit: '3333'
  }
];

describe('<DisplayCards cardlist />', () => {
  const container = shallow(<DisplayCards data={rows} />);

  it('should have a table', () => {
    expect(container.find('table').length).toEqual(1);
  });

  it('should have a tr', () => {
    expect(container.find('tr').length).toEqual(2);
  });

  it('should have a td with the card name', () => {
    expect(
      container
        .find('td')
        .first()
        .text()
    ).toEqual('joe');
  });

  it('should have a td with the card number', () => {
    expect(
      container
        .find('td')
        .at(1)
        .text()
    ).toEqual('1234 5');
  });

  it('should have a td with the card balance', () => {
    expect(
      container
        .find('td')
        .at(2)
        .text()
    ).toEqual('12');
  });

  it('should have a td with the card balance', () => {
    expect(
      container
        .find('td')
        .at(3)
        .text()
    ).toEqual('3333');
  });
});
