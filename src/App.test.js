import React from 'react';
import { shallow } from 'enzyme';
import App from './App';

describe('<App />', () => {
  const container = shallow(<App />);

  it('should have a div', () => {
    expect(container.find('div').length).toEqual(1);
  });

  it('should have a h1', () => {
    expect(container.find('h1').length).toEqual(1);
    expect(container.find('h1').text()).toEqual('Credit Card System');
  });

  it('should have a AddCard element', () => {
    expect(container.find('AddCard').length).toEqual(1);
  });

  it('should have a DisplayCards element', () => {
    expect(container.find('DisplayCards').length).toEqual(1);
  });
});
